<?php

// Execution time
$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 
$totalRecords = 0;

$option_value_id_1 = 60;
$option_value_id_2 = 61;

require_once 'config.php';

$link = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE) or die("Error " . mysqli_error($link)); 


$querySelectProducts = "SELECT * FROM " . DB_PREFIX . "product_option";

$result = $link->query($querySelectProducts);

$deleteOne = "DELETE FROM " . DB_PREFIX . "product_option_value WHERE `option_value_id` = " . $option_value_id_1;
$deleteTwo = "DELETE FROM " . DB_PREFIX . "product_option_value WHERE `option_value_id` = " . $option_value_id_2;

$link -> query($deleteOne);
$link -> query($deleteTwo);


// echo "<pre>";
// print_r($result);
// echo "</pre>";

while($row = mysqli_fetch_array($result)) {
	$proOptId = $row['product_option_id'];
	$proId = $row['product_id'];
	$optId = $row['option_id'];

	$insertOne = "INSERT INTO  `" . DB_PREFIX . "product_option_value` (`product_option_id` ,`product_id` ,`option_id` ,`option_value_id` ,`quantity` ,`subtract` ,`price` ,`price_prefix` ,`points` ,`points_prefix` ,`weight` ,`weight_prefix`)VALUES ('" . $proOptId . "',  '" . $proId . "',  '" . $optId . "',  '" . $option_value_id_1 . "',  '99999',  '1',  '0.8260',  '+',  '0',  '+',  '0.00000000',  '+');";
	$insertTwo = "INSERT INTO  `" . DB_PREFIX . "product_option_value` (`product_option_id` ,`product_id` ,`option_id` ,`option_value_id` ,`quantity` ,`subtract` ,`price` ,`price_prefix` ,`points` ,`points_prefix` ,`weight` ,`weight_prefix`)VALUES ('" . $proOptId . "',  '" . $proId . "',  '" . $optId . "',  '" . $option_value_id_2 . "',  '99999',  '1',  '0.8260',  '+',  '0',  '+',  '0.00000000',  '+');";

	$link -> query($insertOne);
	$link -> query($insertTwo);

	$totalRecords++;
} 


$mtime = microtime(); 
$mtime = explode(" ",$mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 
// echo "This page was created in ".$totaltime." seconds"; 
echo "<h1>DONE!</h1>";
echo "<h3>Total time: " . $totaltime . " seconds</h3>";
echo "<p>Total Records: " . $totalRecords . "</p>";
